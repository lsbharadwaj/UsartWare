#include<main.h>


void DeviceInit()
{
    //--Enable PORT A leds by disabling adc
    ADCON1 = 0xff;
    TRISA = 0x00;
    LATA = 0xFF;
    INTCONbits.GIEH = 1;
    INTCONbits.GIEL = 1;
    RCONbits.IPEN = 1;
    IPR1bits.RCIP = 0;
    IPR1bits.TXIP = 0;
}


void main()
{
    char temp[20];
    DeviceInit();
    usart_open(
        USART_TX_INT_OFF & //0x7f
        USART_RX_INT_ON &  //0xff
        USART_BRGH_HIGH &  //0xff
        USART_EIGHT_BIT & // 0xfd
        USART_ASYNCH_MODE, //0xfe
        1249// 9600 at 48MHz
    );
    //--------------------------------------------//
    stdout = STREAM_USART;
    while(1);
    //--------------------------------------------//
}

void ISRH() __interrupt(1)
{
}

void ISRL() __interrupt(2)
{
    if(PIR1bits.RCIF)
    {
        printf("You typed %c\n",usart_getc());
    }
}
